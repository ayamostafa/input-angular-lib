import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'au-fa-input',
  templateUrl: './au-fa-input.component.html',
  styleUrls: ['./au-fa-input.component.css']
})
export class AuFaInputComponent implements OnInit {
  @Input()
  icon: string;

  constructor() {
  }

  ngOnInit() {
  }

  get classes() {
    const classes = {
      'fa': true
    }
    if (this.icon) {
      classes['fa-' + this.icon] = true;
    }
    return classes;
  }

}
